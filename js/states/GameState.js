var GameState = {
    create: function () {
        //game.stage.backgroundColor = '#65205b';
        game.stage.backgroundColor = '#000000';

        game.groups = {};
        var groups = ['bg', 'enemies', 'player', 'collectibles', 'shots', 'vfx', 'gui'];
        groups.forEach(function(item) {
            game.groups[item] = game.add.group();
        });

        game.player1Ship = new PlayerShip(
            Phaser.Keyboard.LEFT,
            Phaser.Keyboard.RIGHT,
            Phaser.Keyboard.UP,
            Phaser.Keyboard.DOWN,
            Phaser.Keyboard.M,
            Phaser.Keyboard.N,
            'ship1',
            'player1'
        );
        game.player2Ship = new PlayerShip(
          Phaser.Keyboard.J,
          Phaser.Keyboard.L,
          Phaser.Keyboard.I,
          Phaser.Keyboard.K,
          Phaser.Keyboard.U,
          Phaser.Keyboard.O,
          'ship2',
          'player2' 
        );
        game.player3Ship = new PlayerShip(
            Phaser.Keyboard.A,
            Phaser.Keyboard.D,
            Phaser.Keyboard.W,
            Phaser.Keyboard.S,
            Phaser.Keyboard.Q,
            Phaser.Keyboard.E,
            'ship3',
            'player3'
        );
        game.player4Ship = new PlayerShip(
           Phaser.Keyboard.F,
           Phaser.Keyboard.H,
           Phaser.Keyboard.T,
           Phaser.Keyboard.G,
           Phaser.Keyboard.Y,
           Phaser.Keyboard.R,
           'ship4',
           'player4' 
        );
        game.parallax = new Parallax();
        game.hud = new HUD();
        game.spawner = new EnemySpawner();

        /*new AudioSwitch({
            type: 'sound',
            group: game.groups.gui,
            x: 160,
            y: 37,
            atlas: 'atlas',
            spriteOff: 'gui/icon_sound_off',
            spriteOn: 'gui/icon_sound_on'
        });
        new AudioSwitch({
            type: 'music',
            group: game.groups.gui,
            x: 215,
            y: 37,
            atlas: 'atlas',
            spriteOff: 'gui/icon_music_off',
            spriteOn: 'gui/icon_music_on'
        });      
    */
        if (!game.device.desktop) {
            var left = game.add.image(0, game.world.height - 128, 'atlas', 'gui/touch_left');
            left.alpha = 0.2;
            var right = game.add.image(game.world.width - 128, game.world.height - 128, 'atlas', 'gui/touch_right');
            right.alpha = 0.2;
        }
    },
};