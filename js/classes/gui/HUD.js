var HUD = function() {
    this.player1HealthBar = new HealthBar(20, 20);
    this.player2HealthBar = new HealthBar(120, 20);
    this.player3HealthBar = new HealthBar(220, 20);
    this.player4HealthBar = new HealthBar(320, 20);
    somedude='player1';
    console.log(this[`${somedude}HealthBar`]);


    this.score = game.add.text(game.world.width - 50, 56, game.player1Ship.score + game.player2Ship.score + game.player3Ship.score + game.player4Ship.score);
    this.score.anchor.setTo(0.5, 1);
    this.score.align = 'right';
    this.score.fill = '#fff';
    this.score.stroke = '#000';
    this.score.strokeThickness = 4;
    this.score.font = 'square';
    this.score.fontSize = 40;
}

HUD.prototype.update = function(player) {
    this[`${player}HealthBar`].updateHealth(player);
}

HUD.prototype.updateScore = function() {
    this.score.text = game.player1Ship.score + game.player2Ship.score + game.player3Ship.score + game.player4Ship.score;
}